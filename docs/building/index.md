# Building images

The Automotive SIG uses [OSBuild](https://www.osbuild.org/) as the tool to build
its images on CentOS Stream, Fedora, and RHEL hosts, with the option to build immutable images using [OSTree](https://ostreedev.github.io/ostree/introduction/).

!!! note

    For all hosts, installing `osbuild-tools` provides the `osbuild-mpp` utility.

## Prerequisites

- **No cross-compilation**: To build AArch64 or x86_64 images, you must run OSBuild on the respective systems. Some options for AArch64 hosting include Raspberry Pi 4 or qemu on Linux or macOS. For more information, see [AutoSD system running on a Raspberry Pi 4](autosd_pi4.md) or [AutoSD system running on qemu](autosd_qemu.md).

- **A subscribed RHEL system**: To build RHEL images, you must have access to a subscribed RHEL system in order to access to the entitlements. RHEL images can only be built on subscribed RHEL systems.

## Installing osbuild

To configure your system you must install osbuild and the related tools.

!!! note

    We use a newer version of these tools than what is generally available in the distributions, so you must configure a custom repository.

### Installing osbuild on CentOS Stream 8

1. Enable the osbuild repo:
```
dnf copr enable @osbuild/osbuild centos-stream-8-`uname -p`
```
2. Install osbuild and related packages:
```
dnf install osbuild osbuild-tools osbuild-ostree
```

### Installing osbuild on CentOS Stream 9, Fedora, or RHEL

1. Enable the osbuild repo:
```
dnf copr enable @osbuild/osbuild
```
2. Install osbuild and related packages:
```
dnf install osbuild osbuild-tools osbuild-ostree
```

## Finding a manifest

All manifests exist in the project hosted at the [Automotive SIG GitLab](https://gitlab.com/CentOS/automotive/sample-images).

1. Clone the repository to download all manifests:
```
git clone https://gitlab.com/CentOS/automotive/sample-images.git
```
2. Locate the manifests in the `osbuild-manifest` folder. Manifests are organized by OS, then by target platform. For example:
```
    osbuild-manifests
    └── cs9
        ├── minimal.mpp.yml
        └   neptune.mpp.yml
```
Each folder may include a `README` file with information about the differences between the files.

## Adding some dependancies

When building some images, you may require additional Osbuild stages for a successful build. These stages can be provided through installing the `osbuild-auto` package, using the steps below.

1. Enable the copr repository:
```
sudo dnf copr enable -y @centos-automotive-sig/osbuild-auto
```
2. Install the `osbuild-auto` package:
```
sudo dnf install -y osbuild-auto
```

## Building the image

You can build an image manually or by using makefile.

### Building the image manually

Building an image manually is a two-step process.

1. Pass the selected YAML manifest and some options to the `osbuild-mpp` tool. Example:
   ```console
   $ osbuild-mpp images/minimal.mpp.yml /tmp/output.yml -I . -D arch=\"aarch64\"
   ```
 
 This applies the options and resolves the package names against the repositories used to produce a JSON manifest with fully resolved versions of all the packages. This JSON file is fully self contained and produces reproducible builds.

 The manifest has multiple options that can affect how the manifest is preprocessed, for example:

      * `image_type` specifies whether the system is `ostree` or `dnf` based.
      * `extra_rpms` passes extra RPMs to the built image.

2. After preprocessing, pass the resolved manifest to osbuild, which builds the image in a series of steps. Example:
   ```console
   $ sudo osbuild /tmp/output.yml --store store/ --output-directory out/ --export qcow2
   ```

 When you run osbuild you can choose which steps to export. Typically, we export either the step called _image_ or the step called _qcow2_:

      * Image is a raw image that can be written to disk.
      * QCOW2 is a format used by QEMU for image files.

### Using makefile to build the image

You can simplify the build process by using makefile.

!!! note

     Makefile uses sudo, so you may be asked for your password during the build process.

1. From the `osbuild-manifest` directory, run `make`. Be sure to use the correct image name as the target:
```
make cs9-qemu-minimal-regular.x86_64.qcow2
```
 This command preprocesses and builds the manifest for the current architecture and defines the image type to be `ostree`. This results in a file named `cs9-qemu-minimal-regular.x86_64.qcow2`, which is stored in the current directory.

2. You can use makefile to build RAW images as well. To see the full list of images available for the current architecture:
```
make help
```
3. Optional: Remove the `_build` directory to regain disk space:
```
rm -r _build
```
!!! note

    During the build process, artifacts such as JSON files, RPMs, and cached parts from previous builds are stored in the `_build` directory. Everything in this directory is derived from other sources.

### Changing the default password

The sample images use `password` as the default password for `root` and `guest`. You can change the default password when you build the image:
```
DEFINES='root_password="hash_of_new_password"'
```

## Running the image
You can either run the image in QEMU/KVM or flash the image onto an SD card.

### Booting the image in QEMU/KVM

1. Boot the QCOW2 image in `virt-manager` or run it directly through QEMU using the included tool (`runvm`):

If on MacOS, you must install qemu to use runvm via:

```
brew install qemu
```

runvm command:

```
./runvm cs9-qemu-minimal-regular.x86_64.qcow2
```

### Flashing the image onto an SD card

To flash the image onto an SD card:

```
dd if=cs9-qemu-minimal-regular.x86_64.img of=/dev/sda status=progress bs=4M
```
!!! important

    In the example, the block device is called /dev/sda. You must change this to match the block device used in your system. If required, add `-bios /usr/share/OVMF/OVMF_CODE.fd` to QEMU command.

## Using `make manifests`

You can use `make manifests` to preprocess all of the existing manifests without building them. You can use `make manifests` to verify that all combinations of options still work after a change or to inspect the resulting manifests.

In addition to the image types `*.img` and `*.qcow2`, the makefile also supports targets such as:

* `*.rootfs`
* `*.repo`
* `*.tar`
* `*.container`
* `*.ext4`

These targets are useful during development and testing. For more information, run `make help`.

## Building in a virtual machine

The makefile configuration supports running osbuild inside a virtual machine. There are two scenarios in which this is beneficial:

* Standard osbuild requires root/sudo permissions, because it performs some system-level operations such as loopback mounts. Running osbuild in a VM allows you to run it non-privileged.
* When you build an image for a different architecture, you can use QEMU software emulation to make this work. Software emulation is slower than native, but for some use cases it is sufficient.

Running `make osbuildvm-images` uses osbuild on the host to build the supporting image files, `_build/osbuildvm-*`. These files are required to build an image in a VM. The supporting image files can later be used to build other images from the same architecture inside a virtual machine by passing `VM=1` to `make`.

To build images from a different architecture in a VM, there are two options.

1. You may copy the output of `make osbuildvm-images` from a build on a different architecture into `_build`. For example, the contents of _build/osbuildvm-* from an aarch64 machine into _build/ on an x86_64 machine.

2. Alternatively, it is possible to use pre-built osbuildvm images. To build for aarch64 on an x86_64 host, follow the steps outlined below.

    Change to the osbuild-manifests directory
    ```
    cd sample-images/osbuild-manifests/
    ```
    Retrieve the nightly osbuildvm* files and place them into the _build folder
    ```
    wget -r -l 1 -nH -nd -np -A 'osbuildvm*' -P _build https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/
    ```

Once the _build folder is populated with the relevant `osbuildvm-*` files, you may then run `make` with a target that has an architecture different to the host system, for example, make `cs9-qemu-minimal-ostree.aarch64.qcow2`.

!!! note

     You can download pre-built osbuildvm-images files for the aarch64 architecture from the nightly folder at: https://autosd.sig.centos.org/AutoSD-9/nightly/osbuildvm-images/

!!! note

     If you have a previous version of `osbuildvm-images` for a different architecture, you can rebuild or refresh them by using `VM=1` on your architecture.

## Additional resources

For more information, see:

* [Updating an OSTree-based image](updating_ostree.md)
* [Customizing your OSBuild template](customize_template.md)
* [Embedding Containers in images](containers.md)
* [Encrypted images](encryption.md)
* [Using a Raspberry Pi as USB gadget](gadget.md)
* [Boot Time Optimization](boot_time_optimization.md)
* [Automotive Communities](automotive_communities.md)
