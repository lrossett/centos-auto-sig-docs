
# The CentOS Automotive SIG and other communities #

The CentOS Automotive SIG and its resulting AutoSD images propose solutions
answering automotive industry strict demands and also stay friendly with
many automotive FOSS upstream foundations.
The CentOS [Automotive SIG](https://gitlab.com/centos/automotive/sample-images)
repository contains different manifests that may be used to produce different
O/S images based on AutoSD but with customized boots, cpus or image types.
The SIG is also involved with other communities and work done with them can land
in this same repository and this document presents some of this work.

## SOAFEE ##

The Scalable Open Architecture for Embedded Edge (SOAFEE) project is an
industry-led collaboration defined by automakers, semiconductor suppliers,
open source and independent software vendors, and cloud technology leaders.  

The initiative intends to deliver a cloud-native architecture enhanced for
mixed-criticality automotive applications with corresponding open-source
reference implementations to enable commercial and non-commercial offerings,
refer [soafee.io](https://www.soafee.io/).

The CentOS Automotive SIG repository includes manifests for two SOAFEE O/S images
that may be built as Arm O/S with cloud native development and deployment
framework containing RT capabilities ready to launch on AWS as per SOAFEE
blueprints.

* [soafee-manifest][1]:  
one pre-built images from [non-sample-images][2], is ready to use without
building.
* [soafee_r2_manifest][3]:  
support also [soaffee_r2_bp][4] with Autoware [AD-kit][5] by
[Autowhare.org](https://www.autoware.org/autoware).

To build SOAFEE customized boots, cpus or image types please check
list-targets options.

```bash
make list-targets | grep soafee.
```

qemu target:  
**Note**: The architecture will vary depending on the system you run this
command on.

```bash
 make cs9-qemu-soafeer2-ostree.aarch64.qcow2
```

aws target:

```bash
 make cs9-aws-qemu-soafeer2-ostree.aarch64.qcow2

```

### deployment options ###

* Cloud

for AWS deployment, refer
[aws-script][6] exist under repo tools directory

```bash
# export AWS credentials
./tools/export-image-aws.sh <IMAGE_FILE_PATH> <IMPORT_BUCKET_NAME> <AMI_DISK_SIZE_GB>

```

* Virtual Machine

Use builtin [runvm][7] script to run with qemu

```bash
./runvm --verbose cs9-qemu-soafeer2-ostree.aarch64.qcow2
```

#### AD-kit apps ####

Build soafee_r2_manifest with the following command:

```bash
 make cs9-qemu-soafeer2-ostree.x86_aarch64.qcow2
```

Login vm through console

```bash
 podman pod ps
 POD ID        NAME             STATUS      CREATED         INFRA ID      # OF CONTAINERS
 1245b878f8e6  adkit-control    Running     34 seconds ago  340456279f89  2
 e63e7461b7d9  adkit-planning   Running     34 seconds ago  5e3e9ffa9e2d  2
 a77332d48ab8  adkit-map        Running     34 seconds ago  3cd9677e69c1  2
 0ce82085dc95  adkit-simulator  Running     34 seconds ago  b1eaedaa8f45  2
 76441a556edf  adkit-api        Running     34 seconds ago  3e3d7f9263e3  2
 30f97e7c3340  adkit-system     Running     34 seconds ago  61d2a75f54ac  2
 7118bf2528c3  adkit-vehicle    Running     35 seconds ago  cf2fa514df22  2
```

To start simulator, login to simulator container

```bash
podman ps | grep simulator| awk '{print $10}'
adkit-simulator-app
```

Start simulator app

```bash
podman exec -t adkit-simulator-app /bin/bash /opt/adkit/simulator/bin/simulator-run
```

To stop and start services use systemctl command

```bash
systemctl stop adkit-simulator
```

[1]: https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/osbuild-manifests/images/soafee.mpp.yml
[2]: https://sigs.centos.org/automotive/download_images/#non-sample-images
[3]: https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/osbuild-manifests/images/soafeer2.mpp.yml
[4]: https://gitlab.com/soafee/blueprints/-/tree/main/autoware_open_ad_kit_blueprint
[5]: https://www.autoware.org/autoware-open-ad-kit
[6]: https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/osbuild-manifests/tools/export-image-aws.sh
[7]: https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/osbuild-manifests/runvm
